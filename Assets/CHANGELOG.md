# Changelog

## [0.0.3] - 20-09-2019
### Fixed
	-Fix OnCompleted won't be called on UniTask.ToObservable() when UniTask.IsCompleted == true

### Added
	-Add ToUniTask for AssetBundleRequest


## [0.0.2] - 10-08-2019

* Update package directory structure

## [0.0.1] - 04-08-2019

* Initial version

### Features

* 
